#!/bin/bash


curl --request POST --header "PRIVATE-TOKEN: $CI_TOKEN" "https://gitlab.com/api/v4/projects/CI_PROJECT_ID/pipeline?ref=dev" \
--form "variables[][key]=IMAGE" --form "variables[][variable_type]=env_var" --form "variables[][value]=$IMAGE" \
--form "variables[][key]=NGINX" --form "variables[][variable_type]=env_var" --form "variables[][value]=$NGINX"
PIPELINE_ID=$(curl --header "PRIVATE-TOKEN: $CI_TOKEN" "https://gitlab.com/api/v4/projects/CI_PROJECT_ID/pipelines" | jq '.[0] | .id')
JOB_ID=$(curl -k --location --header "PRIVATE-TOKEN: $CI_TOKEN" "https://gitlab.com/api/v4/projects/CI_PROJECT_ID/pipelines/$PIPELINE_ID/jobs" | jq ' .[] | select(.name == "deploy") | .id')
curl -k --request POST --header "PRIVATE-TOKEN: $CI_TOKEN" "https://gitlab.com/api/v4/projects/CI_PROJECT_ID/jobs/$JOB_ID/play"

